#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "lwip/apps/sntp.h"

#include "jsmn.h"

#include "esp_tls.h"

#include "satnogs.h"

#define GROUND_STATION_ID CONFIG_ON_AIR_STATION_ID
#define WEB_SERVER CONFIG_ON_AIR_SERVER
#define WEB_PORT "443"
#define WEB_URL "https://" WEB_SERVER "/api/jobs/"

#define ISO8601_LEN (20)

#define REQ_INTERVAL (1000 * 60 * 5)
#define TOKENS_MAX (512)

static const char *TAG = "satnogs";
static const char *TAG_TLS = "snTLS";

static const char *REQUEST = "GET " WEB_URL "?ground_station=" GROUND_STATION_ID " HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: SatNOGS-On-Air/1.0 esp32\r\n"
    "\r\n";

extern const uint8_t server_root_cert_pem_start[] asm("_binary_server_root_cert_pem_start");
extern const uint8_t server_root_cert_pem_end[]   asm("_binary_server_root_cert_pem_end");

EventGroupHandle_t satnogs_events;
TaskHandle_t request_loop_handle;

void request_loop() {
  int ret;
  int32_t toks;
  jsmn_parser parser;
  jsmntok_t tokens[TOKENS_MAX];
  bzero(&tokens, sizeof(tokens));

  satnogs_status.start = 0;
  satnogs_status.end = 0;

  uint32_t length = 0;
  char *resp = calloc(1, 254);

  while(1) {
      esp_tls_cfg_t cfg = {
          .cacert_pem_buf  = server_root_cert_pem_start,
          .cacert_pem_bytes = server_root_cert_pem_end - server_root_cert_pem_start,
      };
      
      ESP_LOGI(TAG, "creating tls connection");
      struct esp_tls *tls = esp_tls_conn_http_new(WEB_URL, &cfg);
      
      if(tls != NULL) {
          ESP_LOGI(TAG_TLS, "Connection established...");
      } else {
          ESP_LOGE(TAG_TLS, "Connection failed...");
          goto exit;
      }
      
      size_t written_bytes = 0;
      do {
          ret = esp_tls_conn_write(tls, 
                                   REQUEST + written_bytes, 
                                   strlen(REQUEST) - written_bytes);
          if (ret >= 0) {
              ESP_LOGI(TAG, "%d bytes written", ret);
              written_bytes += ret;
          } else if (ret != MBEDTLS_ERR_SSL_WANT_READ  && ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
              ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
              goto exit;
          }
      } while(written_bytes < strlen(REQUEST));

      ESP_LOGI(TAG, "Reading HTTP response...");

      jsmn_init(&parser);

      do {
        ret = esp_tls_conn_read(tls, resp + length, 254);
        
        if(ret == MBEDTLS_ERR_SSL_WANT_WRITE  || ret == MBEDTLS_ERR_SSL_WANT_READ)
          continue;

        ESP_LOGI(TAG, "got %d byes", ret);
        
        if(ret < 0) {
          ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
          break;
        }

        if(ret == 0) {
          ESP_LOGI(TAG, "connection closed");
          break;
        }

        length += ret;
        resp = realloc(resp, 254 + length);
      } while(1);

      ESP_LOGI(TAG, "read %d bytes total", length);
  exit:
      esp_tls_conn_delete(tls);    

      static int request_count;
      ESP_LOGI(TAG, "Completed %d requests", ++request_count);

      ESP_LOGD(TAG, "response:\n>>>>%s<<<<<", resp);
      /* Print response directly to stdout as it is read */

      // instead of doing this here, all content (headers) should
      // be discarded until this is seen
      char *brk = strstr(resp, "\r\n\r\n") + 4;
      
      if(brk != NULL) {
        uint32_t json_len = length - (brk - resp);
        ESP_LOGI(TAG, "JSON length: %d", json_len);

        // figure out why jsmn_parse sometimes returns -1,
        // but then everything else still works
        toks = jsmn_parse(&parser, brk, json_len, tokens, TOKENS_MAX);
        ESP_LOGI(TAG, "Found %d tokens", toks);

        if(toks == -1) {
          ESP_LOGE(TAG, "JSMN_ERROR_NOMEM: Increase TOKENS_MAX");
        } else {
          uint16_t i = 0;
          float diff = 0;
          uint8_t end_idx = 0; // track end key at the same time

          // setup current time
          time_t now;
          struct tm tm_now;

          struct tm tm_pass_start;
          struct tm tm_pass_end;
          time_t pass_start = 0;
          time_t pass_end = 0;

          time(&now);
          localtime_r(&now, &tm_now);
          char strftime_buf[64];

          // XXX ensure this is doing what I think it's doing
          char start_time_buf[ISO8601_LEN + 1];
          char end_time_buf[ISO8601_LEN + 1];

          float soonest_start = -1;

          for(i = 0; i < toks; i++) {
            // might need to track object position
            if(tokens[i].type != JSMN_STRING) continue;

            if(memcmp(brk + tokens[i].start, "start", 5) == 0) {
              // for normal values....
              // i == key, i + 1 == value

              memcpy(start_time_buf, brk + tokens[i + 1].start, ISO8601_LEN);

              ESP_LOGI(TAG, "found start: %s", start_time_buf);

              // 2018-09-07T11:04:52Z
              strptime(start_time_buf, "%Y-%m-%dT%H:%M:%SZ", &tm_pass_start);
              pass_start = mktime(&tm_pass_start);

              diff = difftime(pass_start, now);

              ESP_LOGI(TAG, "pass in %f seconds", diff);

              if(soonest_start == -1 || diff < soonest_start) {
                ESP_LOGI(TAG, "updated soonest: %f", diff);
                // up diff and index of soonest_start pass
                soonest_start = diff;

                // XXX make this DEBUG only
                strftime(strftime_buf, sizeof(strftime_buf), "%c", &tm_pass_start);

                // reset end index
                end_idx = 0;
              } else if(diff > soonest_start) {
                ESP_LOGI(TAG, "later pass? %f (old soonest: %f)", diff, soonest_start);
              }
            }

            if(end_idx == 0 && memcmp(brk + tokens[i].start, "end", 3) == 0) {
              memcpy(end_time_buf, brk + tokens[i + 1].start, ISO8601_LEN);

              ESP_LOGI(TAG, "found end: %s", end_time_buf);
            }
          }

          ESP_LOGI(TAG, "\nJSON Parsing done\n");
          memcpy(&satnogs_status.start, (time_t*)&pass_start, sizeof(time_t));
          memcpy(&satnogs_status.tm_start, (struct tm*)&tm_pass_start, sizeof(struct tm));

          // 2018-09-07T11:04:52Z
          strptime(end_time_buf, "%Y-%m-%dT%H:%M:%SZ", &tm_pass_end);
          pass_end = mktime(&tm_pass_end);

          memcpy(&satnogs_status.end, (time_t*)&pass_end, sizeof(time_t));
          memcpy(&satnogs_status.tm_end, (struct tm*)&tm_pass_end, sizeof(struct tm));

          ESP_LOGI(TAG, "Next pass in %f seconds!", soonest_start);
          ESP_LOGI(TAG, "Date: %s", strftime_buf);

          ESP_LOGD(TAG, "Find end");
        }
      } else {
        ESP_LOGI(TAG, "could not find \\r\\n\\r\\n break in HTTP response");
      }

      length = 0;

      vTaskDelay(1000 * 60 * 1 / portTICK_PERIOD_MS);
      ESP_LOGI(TAG, "Starting again!");
  }
}

void sn_loop() {
  while(true) {
    EventBits_t satnogs_bits = xEventGroupGetBits(satnogs_events);

    // if TIMESET bit NOT set, run clock config
    if(!(satnogs_bits & SATNOGS_TIMESET)) {
      time_t now = 0;
      struct tm timeinfo = { 0 };
      char strftime_buf[64];

      // SNTP started from satnogs_start, wait for it to set the time
      while(timeinfo.tm_year < (2016 - 1900)) {
        ESP_LOGI(TAG, "Waiting for system time to be set...");
        vTaskDelay(2000 / portTICK_PERIOD_MS);

        time(&now);
        localtime_r(&now, &timeinfo);
      }

      // well, what time is it?
      strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
      ESP_LOGI(TAG, "Current time: %s", strftime_buf);

      // set TIMESET bit
      xEventGroupSetBits(satnogs_events, SATNOGS_TIMESET);
    } else if(request_loop_handle == NULL && satnogs_bits & SATNOGS_RUNNING) {
      ESP_LOGI(TAG, "starting request loop");

      // XXX why?
      time_t now;
      time(&now);

      xTaskCreate(&request_loop, "request_loop", 1024 * 15, NULL, 6, &request_loop_handle);
    } else if(request_loop_handle != NULL && !(satnogs_bits & SATNOGS_RUNNING)) {
      // if SatNOGS is stopped, stop the task
      ESP_LOGI(TAG, "stopping request loop");

      vTaskDelete(request_loop_handle);
    } else {
      // this prevents WDT warnings
      vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
  }
}

static void initialize_sntp(void)
{
  // From IDF example code, sets system time
  ESP_LOGI(TAG, "Initializing SNTP");
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, "us.pool.ntp.org");
  sntp_init();
}

void satnogs_stop() {
  if(request_loop_handle != NULL) {
    vTaskDelete(request_loop_handle);
  }
}

EventGroupHandle_t satnogs_init() {
  // EventGroup for tracking state
  satnogs_stop();
  satnogs_events = xEventGroupCreate();
  request_loop_handle = NULL;

  return satnogs_events;
}

void satnogs_start() {
  initialize_sntp();

  ESP_LOGI(TAG, "starting SatNOGS main loop");

  xTaskCreate(&sn_loop, "sn_loop", 4096, NULL, 6, NULL);
}
