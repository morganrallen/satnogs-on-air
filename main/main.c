#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "sdkconfig.h"

#include "esp32-wifi-manager.h"
#include "uart.h"

#include "led_strip/led_strip.h"
#include "satnogs.h"

#define TAG "USER.APP"

#define LED_STRIP_LENGTH 4U
#define LED_STRIP_RMT_INTR_NUM 19U

static EventGroupHandle_t wifi_state;
static EventGroupHandle_t satnogs_state;

static uint8_t id;

void loop(void *p) {
  ESP_LOGI(TAG, "starting main loop");

  while(true) {
    EventBits_t wifi_bits = xEventGroupGetBits(wifi_state);
    EventBits_t satnogs_bits = xEventGroupGetBits(satnogs_state);

    if(wifi_bits & WIFI_CONNECTED && !(satnogs_bits & SATNOGS_RUNNING)) {
      ESP_LOGI(TAG, "WiFi connected, running SatNOGS");

      satnogs_start();
      xEventGroupSetBits(satnogs_state, SATNOGS_RUNNING);
    } else if(!(wifi_bits & WIFI_CONNECTED) && satnogs_bits & SATNOGS_RUNNING) {
      ESP_LOGI(TAG, "WiFi disconnected, stopping SatNOGS");

      xEventGroupClearBits(satnogs_state, SATNOGS_RUNNING);
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

void led_loop() {
	static struct led_color_t led_strip_buf_1[LED_STRIP_LENGTH];
  static struct led_color_t led_strip_buf_2[LED_STRIP_LENGTH];

  struct led_strip_t led_strip = {
    .rgb_led_type = RGB_LED_TYPE_WS2812,
    .rmt_channel = RMT_CHANNEL_1,
    .rmt_interrupt_num = LED_STRIP_RMT_INTR_NUM,
    .gpio = CONFIG_ON_AIR_LED_GPIO,
    .led_strip_buf_1 = led_strip_buf_1,
    .led_strip_buf_2 = led_strip_buf_2,
    .led_strip_length = LED_STRIP_LENGTH
  };
  led_strip.access_semaphore = xSemaphoreCreateBinary();

	bool led_init_ok = led_strip_init(&led_strip);
  ESP_LOGI(TAG, "LED init okay? %s", led_init_ok ? "yes" : "no");

  led_strip_set_pixel_rgb(&led_strip, 1, 0, 0, 42);
  led_strip_show(&led_strip);

  static time_t start = 0;
  static time_t end = 0;

  int8_t running = 0;
  EventBits_t wifi_bits = xEventGroupWaitBits(wifi_state, WIFI_CONNECTED, false, false, portMAX_DELAY);

  ESP_LOGI(TAG, "running led_loop");

  while(true) {
    time_t now;
    struct tm tm_now;
    char strftime_buf[64];

    time(&now);
    localtime_r(&now, &tm_now);

    float time_to_soonest_start  = difftime(satnogs_status.start, now);
    float time_to_soonest_end  = difftime(satnogs_status.end, now);
    float time_to_start  = difftime(start, now);
    float time_to_end  = difftime(end, now);

    ESP_LOGI(TAG, "stored start: %f, satnogs_status.start: %f", time_to_start, time_to_soonest_start);
    ESP_LOGI(TAG, "stored end: %f, satnogs_status.end: %f", time_to_end, time_to_soonest_end);
    ESP_LOGI(TAG, "running? %s", running ? "yes" : "no");

    if(
      (!running && time_to_soonest_end < time_to_end) || // a new pass was scheduled
      (running && time_to_start < 0 && time_to_end < 0) // this pass has completed
    ) {
      ESP_LOGI(TAG, "New soonest pass found, resetting start/end");

      running = 0;
      start = 0;
      end = 0;
    }


    if(start == 0) {
      ESP_LOGI(TAG, "marking start");
      memcpy(&start, (time_t *)&satnogs_status.start, sizeof(time_t));
      time_to_start  = difftime(start, now);
    }

    if(end == 0) {
      ESP_LOGI(TAG, "marking end");
      memcpy(&end, (time_t *)&satnogs_status.end, sizeof(time_t));
      time_to_end  = difftime(end, now);
    }

    if(running) {
      ESP_LOGI(TAG, "running. %f seconds left", time_to_end);
    } else if(time_to_start < 0 && time_to_end > 0) {
      running = 1;
    } else {
      strftime(strftime_buf, sizeof(strftime_buf), "%c", &tm_now);
      ESP_LOGI(TAG, "Pass in: %f seconds", time_to_start);
      ESP_LOGI(TAG, "Now:  %s", strftime_buf);

      strftime(strftime_buf, sizeof(strftime_buf), "%c", &satnogs_status.tm_start);
      ESP_LOGI(TAG, "Pass: %s", strftime_buf);
    }

    if(running) {
      led_strip_set_pixel_rgb(&led_strip, 0, 100, 0, 0);
    } else {
      led_strip_set_pixel_rgb(&led_strip, 0, 10, 24, 74);
    }

    led_strip_show(&led_strip);

    vTaskDelay(1000 * 30 / portTICK_PERIOD_MS);
  }
}

void app_main() {
  esp_err_t ret;

  // Initialize NVS.
  ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK( ret );

  esp_log_level_set(TAG, ESP_LOG_INFO);
  uart_init();

  uint8_t *mac;
  mac = (uint8_t *)malloc(6);
  esp_efuse_mac_get_default(mac);
  id = mac[5];

  ESP_LOGI(TAG, "MAC: %X:%X:%X:%X:%X:%X\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  wifi_state = wifi_manager_start();
  satnogs_state = satnogs_init();

  if(wifi_manager_ap_count() == 0 && strlen(CONFIG_WIFI_MANAGER_TEST_AP) > 0) {
    ESP_LOGI(TAG, "Adding new AP");

    wifi_manager_add_ap(CONFIG_WIFI_MANAGER_TEST_AP, CONFIG_WIFI_MANAGER_TEST_PWD);
  } else {
    ESP_LOGI(TAG, "Got AP from store, waiting for connection");
  }

  wifi_manager_reset_store();
  if(wifi_manager_ap_count() == 0) {
    ESP_LOGI(TAG, "Adding new AP");

    if(strlen(CONFIG_WIFI_MANAGER_TEST_AP) > 0) {
      if(strlen(CONFIG_WIFI_MANAGER_TEST_USER) > 0)
        wifi_manager_add_peap_ap(CONFIG_WIFI_MANAGER_TEST_AP, CONFIG_WIFI_MANAGER_TEST_USER, CONFIG_WIFI_MANAGER_TEST_PWD);
      else
        wifi_manager_add_ap(CONFIG_WIFI_MANAGER_TEST_AP, CONFIG_WIFI_MANAGER_TEST_PWD);
    }

    if(strlen(CONFIG_WIFI_MANAGER_TEST_AP1) > 0) {
      if(strlen(CONFIG_WIFI_MANAGER_TEST_USER1) > 0)
        wifi_manager_add_peap_ap(CONFIG_WIFI_MANAGER_TEST_AP1, CONFIG_WIFI_MANAGER_TEST_USER1, CONFIG_WIFI_MANAGER_TEST_PWD1);
      else
        wifi_manager_add_ap(CONFIG_WIFI_MANAGER_TEST_AP1, CONFIG_WIFI_MANAGER_TEST_PWD1);
    }

    if(strlen(CONFIG_WIFI_MANAGER_TEST_AP2) > 0) {
      if(strlen(CONFIG_WIFI_MANAGER_TEST_USER2) > 0)
        wifi_manager_add_peap_ap(CONFIG_WIFI_MANAGER_TEST_AP2, CONFIG_WIFI_MANAGER_TEST_USER2, CONFIG_WIFI_MANAGER_TEST_PWD2);
      else
        wifi_manager_add_ap(CONFIG_WIFI_MANAGER_TEST_AP2, CONFIG_WIFI_MANAGER_TEST_PWD2);
    }
  } else {
    ESP_LOGI(TAG, "Got AP from store, waiting for connection");
  }

  xTaskCreate(&led_loop, "led_loop",  2048, NULL, 6, NULL);
  xTaskCreate(&loop, "loop",  2048, NULL, 6, NULL);
};
