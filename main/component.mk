#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

EXTRA_COMPONENT_DIRS := ../lib/ESP32_LED_STRIP/components/
COMPONENT_SRCDIRS := . ../lib/jsmn ../lib/ESP32_LED_STRIP/components/led_strip/
COMPONENT_ADD_INCLUDEDIRS := ../include ../lib/ESP32_LED_STRIP/components/led_strip/inc/
COMPONENT_EMBED_TXTFILES := server_root_cert.pem
