#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "driver/uart.h"
#include "uart.h"

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
#define EX_UART_NUM UART_NUM_0

#define ECHO_UART0  (true)

static QueueHandle_t uart0_queue;

static void uart_event_task(void *pvParameters) {
  uart_event_t event;
  uint8_t *dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
  uint8_t size = 0;

  bzero(dtmp, RD_BUF_SIZE);

  for(;;) {
    if(xQueueReceive(uart0_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
      switch(event.type) {
        case UART_DATA:
          uart_read_bytes(UART_NUM_0, dtmp + size, event.size, portMAX_DELAY);
#ifdef ECHO_UART0          
          uart_write_bytes(UART_NUM_0, (const char*) dtmp + size, event.size);
#endif

          size += event.size;

          if(dtmp[size - 1] == '\n') {
            uart_write_bytes(UART_NUM_0, (const char*) dtmp, size);

            bzero(dtmp, RD_BUF_SIZE);
            size = 0;
          }

          break;

          case UART_BREAK:
          case UART_BUFFER_FULL:
          case UART_FIFO_OVF:
          case UART_FRAME_ERR:
          case UART_PARITY_ERR:
          case UART_DATA_BREAK:
          case UART_PATTERN_DET:
          case UART_EVENT_MAX:
          break;
      }
    }
  }
}

void uart_init() {
  uart_config_t uart_config = {
    .baud_rate = 115200,
    .data_bits = UART_DATA_8_BITS,
    .parity = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
  };

  ESP_ERROR_CHECK(uart_param_config(EX_UART_NUM, &uart_config));
  ESP_ERROR_CHECK(uart_set_pin(EX_UART_NUM, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));
  ESP_ERROR_CHECK(uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0));

  //Create a task to handler UART event from ISR
  xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 1, NULL);
}
