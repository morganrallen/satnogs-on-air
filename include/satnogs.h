#ifndef __SATNOGS_H_
#define __SATNOGS_H_

#define SATNOGS_RUNNING (BIT0)
#define SATNOGS_TIMESET (BIT1)

struct satnogs_status {
  time_t start;
  time_t end;

  struct tm tm_start;
  struct tm tm_end;
} satnogs_status;

EventGroupHandle_t satnogs_init();
void satnogs_stop();
void satnogs_start();

#endif
