# SatNOGS On Air
Display the status of a SatNOGS ground station.


# Quick Start
```
git clone https://gitlab.com/morganrallen/satnogs-on-air.git
cd satnogs-on-air
make defconfig # may need to run make menuconfig for your specific device
make flash
make monitor
```
