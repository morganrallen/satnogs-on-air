#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := satnogs-on-air
ADD_INCLUDEDIRS := main include lib/jsmn lib/ESP32_LED_STRIP/components/led_strip/inc

include $(IDF_PATH)/make/project.mk
